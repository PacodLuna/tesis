import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace org.acme.mynetwork{
   export enum CarState {
      FOR_SALE,
      NOT_FOR_SALE,
   }
   export class Car extends Asset {
      carSymbol: string;
      description: string;
      make: string;
      model: string;
      state: CarState;
      price: number;
      owner: Person;
   }
   export class Person extends Participant {
      personId: string;
      firstName: string;
      lastName: string;
      currency: number;
   }
   export class Trade extends Transaction {
      car: Car;
      newOwner: Person;
   }
   export class AddCurrency extends Transaction {
      money: number;
      owner: Person;
   }
// }
