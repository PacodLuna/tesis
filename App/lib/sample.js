'use strict';
var NS = 'org.acme.mynetwork';

/**
 * Track the trade of a car from one trader to another
 * @param {org.acme.mynetwork.Trade} trade - the trade to be processed
 * @transaction
 */
function tradeCar(trade) {
    trade.car.owner = trade.newOwner;
    return getAssetRegistry('org.acme.mynetwork.Car')
        .then(function (assetRegistry) {
            return assetRegistry.update(trade.car);
        });
}

/**
 * Add Currency to a Person
 * @param {org.acme.mynetwork.AddCurrency} qm 
 * @transaction
 */
function addCurrency(qm){
    qm.owner.currency = qm.money + qm.owner.currency;
    return getParticipantRegistry('org.acme.mynetwork.Person')
        .then(function (participantRegistry) {
            return participantRegistry.update(qm.owner);
        });
}

/**
 * Substract Currency to a Person
 * @param {org.acme.mynetwork.SubstractCurrency} qs
 * @transaction
 */
function addCurrency(qs){
    qs.owner.currency = qm.money - qm.owner.currency;
    return getParticipantRegistry('org.acme.mynetwork.Person')
        .then(function (participantRegistry) {
            return participantRegistry.update(qm.owner);
        });
}

/**
 * Sell a car
 * @param {org.acme.mynetwork.Trade} trade - the trade to be processed
 * @transaction
 */
function sellCar(trade){
    
    if(trade.newOwner.currency > trade.car.price){
        trade.car.owner.currency = trade.car.owner.currency + trade.car.price; 
        trade.newOwner.currency = trade.newOwner.currency - trade.car.price;
        /*let p1registry = await getParticipantRegistry('org.acme.mynetwork.Person');
        await p1registry.update(trade.car.owner);
        await p1registry.update(trade.newOwner);*/
        trade.car.owner = trade.newOwner;
        return getAssetRegistry('org.acme.mynetwork.Car')
            .then(function (assetRegistry) {
                return assetRegistry.update(trade.car);
            });
    }
}
